﻿namespace TelegramBot.Lesson
{
    internal class Photo
    {
        public string Caption { get; set; }

        public string FileId { get; set; }
    }
}